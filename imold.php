<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'header.php'; ?>
    <!-- Bootstrap core CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/about.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="nav navbar-nav navbar-brand">
                <li class="sk">
                    <a href="index.php" class="brandsk" style="float:left;padding-top: 0px;padding-bottom: 15px;">SAQIB
                        KHAN</a>
                </li>
            </ul>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="blog.php">&nbsp;&nbsp;&nbsp;BLOG&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="punch_it.php">&nbsp;&nbsp;&nbsp;PUNCH IT&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="invite.php">&nbsp;&nbsp;&nbsp;INVITE TO SPEAK&nbsp;&nbsp;&nbsp;</a></li>
                <li class="active"><a href="lets_meet.php"> &nbsp;&nbsp;&nbsp;Let's Meeeet&nbsp;&nbsp;&nbsp;</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/about.jpg');height:400px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Reach Me</h1>
                    <hr class="small">
                    <span class="subheading">And nasmnjdas  dzmcnjdknksas kdks</span>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="center">
        <h2>Drop Your Message</h2>
        <p class="lead">We promise we will get back to you promptly. Don't hesistate to contact us for further
            enquiry.</p>
    </div>
    <div class="row contact-wrap">
        <div class="status alert alert-success" style="display: none"></div>
        <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php">
            <div class="col-sm-5 col-sm-offset-1">
                <div class="form-group">
                    <label>Name *</label>
                    <input type="text" name="name" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Email *</label>
                    <input type="email" name="email" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="number" name="phone" class="form-control">
                </div>
                <div class="form-group">
                    <label>College Name</label>
                    <input type="text" name="college" class="form-control">
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <label>Subject *</label>
                    <input type="text" name="subject" class="form-control" required="required">
                </div>
                <div class="form-group">
                    <label>Message *</label>
                    <textarea name="message" id="message" required="required" class="form-control" rows="8"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-primary btn-lg">Submit Message</button>
                </div>
            </div>
        </form>
    </div>
    <!--/.row-->
</div>
<!--/.container-->
<!-- FOOTER -->
<div id="f">
    <center>
        <div class="container">
            <div class="row top40">
                <DIV id="ftext" class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                    <p><b>STAY IN <BR>
                            <br>
                            TOUCH<BR><br>
                            WITH<BR><br>
                            <font color="red"> ME</font></b>
                    </P>
                </DIV>
                <div class="row centered col-xs-10 col-sm-10 col-md-10 col-lg-10;">
                    <ul>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li><a href="">
                                <i style="background:#3b5998" class="fa fa-facebook"></i>
                                <strong id="fstext"><br>Discuss with <br>me and ask <br>Questions</strong>
                            </a>
                        </li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li>
                            <a href="">
                                <i style="background:#00aced" class="fa fa-twitter"></i>
                                <strong id="fstext"><br>The best way <br>to talk to me <br>directly and connect<br>with
                                    me</strong>
                            </a>
                        </li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <!--<li><a href=""><i class="fa fa-snapchat" style="font-size:50px;color:yellow"></i></a></li>-->
                        <li>
                            <a href=""><i style="background-color:#bc2a8d;" class="fa fa-instagram"></i>
                                <strong id="fstext"><br>My pictures<br>and some <br>inspiring quotes</strong>
                            </a>
                        </li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li>
                            <a href="">
                                <i style="background:#bb0000" class="fa fa-youtube"></i>
                                <strong id="fstext"><br>My videos, <br>My Lessons,<br>My Journey &amp; <br>#askthekhan
                                    show</strong>
                            </a>
                        </li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    </ul>
                </div>
                <!-- row -->
            </div>
            <!-- container -->
    </center>
</div><!-- Footer -->
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>