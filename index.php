<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'header.php'; ?>
    <!-- Bootstrap core CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/punchit.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="nav navbar-nav navbar-brand">
                <li class="sk">
                    <a href="index.php" class="brandsk" style="float:left;padding-top: 0px;padding-bottom: 15px;">SAQIB
                        KHAN</a>
                </li>
            </ul>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="blog.php">&nbsp;&nbsp;&nbsp;BLOG&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="punch_it.php">&nbsp;&nbsp;&nbsp;PUNCH IT&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="invite.php">&nbsp;&nbsp;&nbsp;INVITE TO SPEAK&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="lets_meet.php"> &nbsp;&nbsp;&nbsp;LET'S MEET&nbsp;&nbsp;&nbsp;</a></li>
            </ul>
        </div>
    </div>
</nav>
<div id="headerwrap">
    <div class="container">
        <div class="row centered">
            <form method="post" action="about.php">
                <div class="firsttime">
                    <button style="margin-top:375px" class="btn-effect" type="submit"><b>First Time Here</b></button>
                </div>
            </form>
        </div>
        <!-- row -->
    </div>
    <!-- container -->
</div>
<!-- headerwrap -->
<!--Subscribe-->
<div class="subscribe" style="background: url(img/subscribe.jpg)no-repeat center bottom #C0392B;">
    <div class="container">
        <h1>CHANGE YOUR LIFE FULLY</h1>
        <h3>GET AN EXCLUSIVE LIFE LESSON RIGHT IN YOUR MAILBOX EVERY MONDAY.</h3>
        <form>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <input class="input-lg" type="text" placeholder="Enter your name">&nbsp&nbsp
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                    <input class="input-lg" type="text" placeholder="Enter email">&nbsp&nbsp
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <button class="btn-effect" type="submit"><b>Subscribe</b></button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End subscribe -->
<br>
<br>
<!--Start of Part 3 -->
<div class="container">
    <div class="row top40">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="team">
                <h1><strong>SAQIB KHAN BUILDING LIVES & SPREADING SMILES</strong></h1>
                <p><b>Saqib Khan is a Trainer, Life & Success Coach, Motivational Speaker & loves to help
                        people. An Engineer by profession but a hardcore people’s person in passion,
                        Saqib has brought about a major change in the way trainings are conducted.</b>
                </p>
                <P><B>Passed out of college only in 2014, Saqib is a young & dynamic public speaker who
                        boasts of a beautiful command over the people he coaches. His training programmes are
                        full of knowledge and just super exciting to attend. He works mainly with youth across
                        different genres like young Entrepreneurs, youth leaders, college student, thinkers etc.
                        You will never go bored or uninspired from the longest of his sessions.</B>
                </P>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="team">
                <div class="video-container">
                    <iframe width="853" height="500" src="https://www.youtube.com/embed/ZnfZUhyBHhk" frameborder="0"
                            allowfullscreen></iframe>
                </div>
                <BR>
            </div>
        </div>
    </div>
</div>
<!--End of Part 3 -->
<!--START OF BLOG SECTION -->
<section id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>
                        <b>
                            <center>Blog</center>
                            <b>
                    </h1>
                    <hr class="small">
                    <!--   <span class="subheading">Trending posts</span> -->
                </div>
            </div>
        </div>
        <div class="portfolio-items">
            <div class="row">
                <div class="portfolio-item happy col-xs-12 col-sm-4 col-md-3">
                    <img class="img-responsive" style="padding-left:2px;padding-right:20px" src="img/hmblog/failure.jpg"
                         alt="">
                    <div class="overlay">
                        <div class="recent-work-inner">
                            <h3><a href="#"><b>The reason you fail</b></a></h3>
                        </div>
                    </div>
                    <br>
                    <br>
                </div><!--/.portfolio-item-->
                <div class="portfolio-item happy col-xs-12 col-sm-4 col-md-3">
                    <img class="img-responsive" style="padding-left:2px;padding-right:20px" src="img/hmblog/male.jpeg"
                         alt="">
                    <div class="overlay">
                        <div class="recent-work-inner">
                            <h3><a href="#"><b>Let’s think of the male community too </b></a></h3>
                        </div>
                    </div>
                    <br>
                    <br>
                </div><!--/.portfolio-item-->
                <div class="portfolio-item others col-xs-12 col-sm-4 col-md-3">
                    <img class="img-responsive" style="padding-left:2px;padding-right:20px" src="img/hmblog/happy.jpg"
                         alt="">
                    <div class="overlay">
                        <div class="recent-work-inner">
                            <h3><a href="#"><b>Be happy with yourself.. </b></a></h3>
                        </div>
                    </div>
                    <br>
                    <br>
                </div><!--/.portfolio-item-->
                <div class="portfolio-item sad col-xs-12 col-sm-4 col-md-3">
                    <img class="img-responsive" style="padding-left:2px;padding-right:20px" src="img/hmblog/leaders.jpg"
                         alt="">
                    <div class="overlay">
                        <div class="recent-work-inner">
                            <h3><a href="#"><b>True leaders do this..</b></a></h3>
                        </div>
                    </div>
                </div>
            </div><!--/.portfolio-item-->
        </div>
        <center>
            <form method="post" action="blog.php">
                <div class="firsttime">
                    <H1>Click here to </h1>
                    <button class="btn-effect" type="submit"><b>Read More</b></button>
                </div>
            </form>
        </center>
        </b>
    </div>
</section>
<!--/#portfolio-item-->
<!--End of Blog-->
<!--Sucess stories Start-->
<div class="container" style="padding-bottom:15px;padding-top:15px;">
    <h3><b>Success Stories</b></h3>
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
            <li data-target="#myCarousel" data-slide-to="7"></li>
            <li data-target="#myCarousel" data-slide-to="8"></li>
            <li data-target="#myCarousel" data-slide-to="9"></li>
            <li data-target="#myCarousel" data-slide-to="10"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="img\ss\1.jpg" alt="1">
            </div>
            <div class="item">
                <img src="img\ss\2.jpg" alt="2">
            </div>
            <div class="item">
                <img src="img\ss\3.jpg" alt="3">
            </div>
            <div class="item">
                <img src="img\ss\4.jpg" alt="4">
            </div>
            <div class="item">
                <img src="img\ss\5.jpg" alt="5">
            </div>
            <div class="item">
                <img src="img\ss\6.jpg" alt="6">
            </div>
            <div class="item">
                <img src="img\ss\7.jpg" alt="7">
            </div>
            <div class="item">
                <img src="img\ss\8.jpg" alt="8">
            </div>
            <div class="item">
                <img src="img\ss\9.jpg" alt="Flower">
            </div>
            <div class="item">
                <img src="img\ss\10.jpg" alt="Flower">
            </div>
            <div class="item">
                <img src="img\ss\11.jpg" alt="Flower">
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control"
           style="background-image: linear-gradient(to right,rgba(0,0,0,0) 0,rgba(0,0,0,0) 100%);" href="#myCarousel"
           role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control"
           style="background-image: linear-gradient(to left,rgba(0,0,0,0) 0,rgba(0,0,0,0) 100%);" href="#myCarousel"
           role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<script>
    $(document).ready(function () {
        // Activate Carousel
        $("#myCarousel").carousel();

        // Enable Carousel Indicators
        $(".item1").click(function () {
            $("#myCarousel").carousel(0);
        });
        $(".item2").click(function () {
            $("#myCarousel").carousel(1);
        });
        $(".item3").click(function () {
            $("#myCarousel").carousel(2);
        });
        $(".item4").click(function () {
            $("#myCarousel").carousel(3);
        });

        // Enable Carousel Controls
        $(".left").click(function () {
            $("#myCarousel").carousel("prev");
        });
        $(".right").click(function () {
            $("#myCarousel").carousel("next");
        });
    });
</script>
<!--Sucess stories END-->
<!--IPLINK STARTS-->
<div class="col-md-12 subscribe" style="background-image:url(img/iplink.png); background-size:cover;">
    <div class="container">
        <h1>DO ONE OF THE TWO THINGS RIGHT NOW</h1>
        <div class="col-md-6">
            <div class="panel" style="background-image:url(img/ipk2.jpg); ">
                <div class="panel-body">
                    <h3>GET AN AUDIO EXPERIENCE</h3>
                    <br/>
                    <center><a href="punch_it.php">
                            <button class="btn btn-success btn-lg" type="button">PUNCH IT</button>
                        </a>
                    </center>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel" style="background-image:url(img/ipk2.jpg); ">
                <div class="panel-body">
                    <h3>LISTEN TO ME TALK LIVE</h3>
                    <br/>
                    <br/>
                    <center><a href="invite.php">
                            <button class="btn btn-success btn-lg" type="button">INVITE ME</button>
                        </a>
                    </center>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>
<!--IPLINK ends-->
<?php include_once 'footer.php'; ?>
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>