<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'header.php'; ?>
    <!-- Bootstrap core CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/about.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
        .vertical .carousel-inner {
            height: 100%;
        }

        .carousel.vertical .item {
            -webkit-transition: 0.6s ease-in-out top;
            -moz-transition: 0.6s ease-in-out top;
            -ms-transition: 0.6s ease-in-out top;
            -o-transition: 0.6s ease-in-out top;
            transition: 0.6s ease-in-out top;
        }

        .carousel.vertical .active {
            top: 0;
        }

        .carousel.vertical .next {
            top: 400px;
        }

        .carousel.vertical .prev {
            top: -400px;
        }

        .carousel.vertical .next.left,
        .carousel.vertical .prev.right {
            top: 0;
        }

        .carousel.vertical .active.left {
            top: -400px;
        }

        .carousel.vertical .active.right {
            top: 400px;
        }

        .carousel.vertical .item {
            left: 0;
        }
    </style>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            display: block;
            max-width: 100%;
            height: 400px !important;
        }
    </style>
</head>
<body>
<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="nav navbar-nav navbar-brand">
                <li class="sk">
                    <a href="index.php" class="brandsk" style="float:left;padding-top: 0px;padding-bottom: 15px;">SAQIB
                        KHAN</a>
                </li>
            </ul>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="blog.php">&nbsp;&nbsp;&nbsp;BLOG&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="punch_it.php">&nbsp;&nbsp;&nbsp;PUNCH IT&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="invite.php">&nbsp;&nbsp;&nbsp;INVITE TO SPEAK&nbsp;&nbsp;&nbsp;</a></li>
                <li class="active"><a href="lets_meet.php"> &nbsp;&nbsp;&nbsp;LET'S MEET&nbsp;&nbsp;&nbsp;</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/about.jpg');height:400px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Reach Me</h1>
                    <hr class="small">
                    <span class="subheading"></span>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-md-9 col-sm-12">
            <div>
                <BR>
                <p style="font-size:32px;"><b>
                        Never think that no one wants to listen to you. We are there to talk. Talk business,
                        startup, Leadership, Strategy, designing, Life, success, career and so many other things
                        one on one with utmost privacy. Get some handy tips and guidance exclusively made for
                        you. One meetup and you will be more than eager for more such meetups.
                </p>
                </b>
                <BR>
                <div class="panel" style="background-image:url(img/iplink.png);font-color:#fff;">
                    <div class="panel-body">
                        <strong>
                            <h1 style="font-size:32px;">
                                <center>
                                    GET TO KNOW MORE FROM PDF
                                    <center>
                            </h1>
                        </strong>
                        <br/>
                        <center><a href="punch_it.php">
                                <button class="btn btn-success btn-lg" type="button">KNOW MORE</button>
                            </a>
                        </center>
                    </div>
                </div>
                <BR>
                <div class="panel subscribe" style="background-image:url(img/subscribe.jpg);font-color:#fff;">
                    <div class="panel-body">
                        <div class="center ">
                            <h1>WANT TO TALK TO ME? </h1>
                            <br>
                            <h3>If you are not sure, fill the form to schedule a free 15 min call with Saqib Khan
                                himself. Use the #freefirst15 in description field.</h3>
                            <br>
                            <div class="row contact-wrap">
                                <div class="status alert alert-success" style="display: none"></div>
                                <form id="main-contact-form" class="contact-formrounded" name="contact-form"
                                      method="post" action="sendemail.php">
                                    <div class="col-sm-5 col-sm-offset-1">
                                        <div class="form-group">
                                            <label>
                                                <b>
                                                    <h4 style="color:#fff">Name *</h4>
                                                </b>
                                            </label>
                                            <input type="text" name="name" class="form-control" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <b>
                                                    <h4 style="color:#fff">Email *</h4>
                                                </b>
                                            </label>
                                            <input type="email" name="email" class="form-control" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <b>
                                                    <h4 style="color:#fff">Phone</h4>
                                                </b>
                                            </label>
                                            <input type="number" name="phone" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <b>
                                                    <h4 style="color:#fff">Organisation Name</h4>
                                                </b>
                                            </label>
                                            <input type="text" name="college" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>
                                                <b>
                                                    <h4 style="color:#fff">Subject *</h4>
                                                </b>
                                            </label>
                                            <input type="text" name="subject" class="form-control" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label>
                                                <b>
                                                    <h4 style="color:#fff">Message *</h4>
                                                </b>
                                            </label>
                                            <textarea name="message" id="message" required="required"
                                                      class="form-control" rows="8"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" id="submit_form" name="submit"
                                                    class="btn btn-primary btn-lg">Submit Message
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <div id="submit_message"></div>
                            </div>
                        </div>
                    </div>
                    <!--/.row-->
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12" style="padding-left:5px;padding-right:5px;">
            <h1 style="font-size:42px"><strong>WANT TO KNOW SOME ANSWER</strong></h1>
            <h3 style="font-size:42px"><strong></strong></h3>
            <BR>
            <div class="panel" style="background-image:url(img/ipk2.jpg); ">
                <div class="panel-body">
                    <h3 style="font-size:42px"><strong style="color:#fff">FEEL FREE TO ASK</strong></h3>
                    <BR>
                    <br/>
                    <P style="color:#fff">POST YOUR QUERY AT FACEBOOK WITH ANONYMITY
                        GET YOUR ANSWER AND SOLUION FROM ME
                    </P>
                    <center><a href="https://www.facebook.com/saqibkhanspeaks/">
                            <button class="btn btn-success btn-lg" type="button">#ASK THE KHAN</button>
                        </a>
                    </center>
                </div>
            </div>
            <hr>
            <h1 style="font-size:42px"><strong>CATCH ME HERE</strong></h1>
            <BR>
            <div class="row-fluid">
                <div class="span6 offset3">
                    <div id="myCarousel" class="carousel slide vertical">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="active item">
                                <img src="img/vs/01.jpg">
                            </div>
                            <div class="item">
                                <img src="img/vs/02.jpg">
                            </div>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a>
                        <a class="carousel-control right" href="#myCarousel" data-slide="next">›</a>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('.carousel').carousel({
                interval: 3000
            })
        </script>
        <!--/#portfolio-item-->
    </div>
</div>
</section>
<!--Social Media Icon Section Starts-->
<?php include_once 'footer.php'; ?>
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    $('#submit_form').click(function () {
        //var flag = validateFrom();
        var flag = true;
        if (flag) {
            $.ajax({
                url: "ajax.php?action=lets_meet&" + $('#main-contact-form').serialize(),
                /*data: {
            Name  : $('#Name').val(),
            college : $('#college').val(),
            email : $('#email').val(),
            phone : $('#phone').val(),
            sel1  : $('#sel1').val(),
            message : $('#message').val()
                },*/
                dataType: 'json',
                success: function (data) {
                    if (data == true) {
                        $('#submit_message').html('<div class="alert alert-success" role="alert">We have received your mail will get back to you soon.</div>');
                    } else {
                        $('#submit_message').html('<div class="alert alert-warning" role="alert">Failed to send mail please try again after sometime</div>');
                    }
                }
            });
        }
    })
</script>
</body>
</html>