<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'header.php'; ?>
    <!-- Bootstrap core CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/punchit.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <![endif]-->
</head>
<body style="background-color: white">
<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="nav navbar-nav navbar-brand">
                <li class="sk">
                    <a href="index.php" class="brandsk" style="float:left;padding-top: 0;padding-bottom: 15px;">SAQIB
                        KHAN</a>
                </li>
            </ul>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="blog.php">&nbsp;&nbsp;&nbsp;BLOG&nbsp;&nbsp;&nbsp;</a></li>
                <li class="active"><a href="punch_it.php">&nbsp;&nbsp;&nbsp;PUNCH IT&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="invite.php">&nbsp;&nbsp;&nbsp;INVITE TO SPEAK&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="lets_meet.php"> &nbsp;&nbsp;&nbsp;LET'S MEET&nbsp;&nbsp;&nbsp;</a></li>
            </ul>
        </div>
    </div>
</nav>
<header id="image"></header>
<header class="mood" style="background: black;">
    <div class="container">
        <h1>YOUR THOUGHTS CAN CHANGE YOUR LIFE</h1>
        <div class="dropdown text-center">
            <button class="button" type="button" data-toggle="dropdown" style="align-self: center;">Choose what best describes your
                thoughts
            </button>
            <ul class="dropdown-menu dropdown-menu-center">
                <li>
                    <h4>Happy: When you are happy and you know it</h4>
                </li>
                <li class="divider"></li>
                <li>
                    <h4>Sad: If you wanna cry right now</h4>
                </li>
                <li class="divider"></li>
                <li>
                    <h4>Inspired: Let’s get shit done</h4>
                </li>
                <li class="divider"></li>
                <li>
                    <h4>Thoughtfull: I am all about business baby</h4>
                </li>
                <li class="divider"></li>
                <li>
                    <h4>Energetic: Catch me if you can</h4>
                </li>
                <li class="divider"></li>
                <li>
                    <h4>Depressed: Life is all over</h4>
                </li>
                <li class="divider"></li>
                <li>
                    <h4>Anxious: I have no clue what happens next</h4>
                </li>
                <li class="divider"></li>
                <li>
                    <h4>Confused: this thing.. no that thing… hold on maybe..arghhh.. I don’t know</h4>
                </li>
            </ul>
        </div>
    </div>
</header>
<div class="container-fluid">
    <br><br>
    <div class="row">
        <div class="portfolio-items col-sm-9" style="background-color:white;">
            <div class="row">
                <div class="portfolio-item col-sm-4">
                <img class="img-responsive" src="img/punch/business.jpeg" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business Options</a></h3>
                        <audio controls>
                            <source src="podcast/Business Options _Inspiring_business.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/Failed in life.jpeg" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Failed in Life</a></h3>
                        <audio controls>
                            <source src="podcast/Failed in Life_sad_inspiring.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/game is strength.jpg" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Game is strength</a></h3>
                        <audio controls>
                            <source src="podcast/Game is strength_Inspiring.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/power of belief.jpeg" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Power of Belief</a></h3>
                        <audio controls>
                            <source src="podcast/Power of Belief_happy_inspiring.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/recent/item3.png" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business theme</a></h3>
                        <audio controls>
                            <source src="podcast/1.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/struggle.jpeg" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Struggle</a></h3>
                        <audio controls>
                            <source src="podcast/Struggle_sad_happy_inspiring.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/try dont cry.jpg" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Try,Don't Cry</a></h3>
                        <audio controls>
                            <source src="podcast/Try,Don't Cry_sad_inspiring.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/recent/item5.png" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business theme</a></h3>
                        <audio controls>
                            <source src="podcast/1.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/recent/item5.png" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business theme</a></h3>
                        <audio controls>
                            <source src="podcast/1.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/recent/item5.png" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business theme</a></h3>
                        <audio controls>
                            <source src="podcast/1.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/recent/item5.png" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business theme</a></h3>
                        <audio controls>
                            <source src="podcast/1.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/recent/item5.png" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business theme</a></h3>
                        <audio controls>
                            <source src="podcast/1.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/recent/item5.png" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business theme</a></h3>
                        <audio controls>
                            <source src="podcast/1.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/recent/item5.png" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">Business theme</a></h3>
                        <audio controls>
                            <source src="podcast/1.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
            <div class="portfolio-item col-sm-4" style="background-color:white;">
                <img class="img-responsive" src="img/punch/the game called competiton.jpg" alt="">
                <div class="overlay">
                    <div class="recent-work-inner">
                        <h3><a href="#">The Competition Game</a></h3>
                        <audio controls>
                            <source src="podcast/The game called Competition_happy_sad.mp3" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3" style="background-color:black;">
        <h2><font color="white">Invite Me to Speak</font></h2>
        <h3><font color="white">If you like my talks, you can call me to conduct the session.</font></h3>
        <h3><a href="invite.php" style="color:white;">Contact me</a></h3>
        <br>
    </div>
    <div class="col-sm-3" style="background-color:white;"><br><br></div>
    <div class="col-sm-3" style="background-color:black;">
        <h4><font color="white">Get Exclusive content in your mailbox</font></h4>
        <h3><font color="white">Subscribe:</font></h3>
        <form>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <input class="input-lg" type="text" placeholder="Enter your name" size="30">
                </div>
                <br><br><br>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <input class="input-lg" type="text" placeholder="Enter email" size="30">
                </div>
                <br><br><br>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <button class="btn-effect" type="submit"><b>Submit</b></button>
                    <br><br>
                </div>
            </div>
        </form>
    </div>
</div>
<?php include_once 'footer.php'; ?>
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/jquery.js"></script>

<script>
    $(document).ready(function () {
       getPodCast();
    });
    function getPodCast() {
        var podCast_id = '';
        var category_id = '';
        $.ajax({
            url: "ajax.php?action=get_podCasts&podCast_id="+podCast_id+"&category_id="+category_id,
            dataType: 'json',
            success: function (data) {
                console.log(data);
            }
        })
    }
</script>

</body>
</html>