<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'header.php'; ?>
    <!-- Bootstrap core CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/punchit.css" rel="stylesheet">
    <link href="css/about.css" rel="stylesheet">
    <style>
        @media only screen and (min-width: 900px) {
            iframe {
                width: 75%;
                height: 75%;
            }
        }
    </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="nav navbar-nav navbar-brand">
                <li class="sk">
                    <a href="index.php" class="brandsk" style="float:left;padding-top: 0px;padding-bottom: 15px;">SAQIB
                        KHAN</a>
                </li>
            </ul>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="blog.php">&nbsp;&nbsp;&nbsp;BLOG&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="punch_it.php">&nbsp;&nbsp;&nbsp;PUNCH IT&nbsp;&nbsp;&nbsp;</a></li>
                <li class="active"><a href="invite.php">&nbsp;&nbsp;&nbsp;INVITE TO SPEAK&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="lets_meet.php"> &nbsp;&nbsp;&nbsp;LET'S MEET&nbsp;&nbsp;&nbsp;</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('img/invite.jpg');height:400px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="page-heading">
                    <h1>Invite Me to speak</h1>
                    <hr class="small">
                    <span class="subheading">This what i do</span>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- end of header-->
<!---->
<section class="section-padding wow fadeInUp delay-02s" id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <div class="section-title">
                    <h2 class="head-title"><strong>INSTAFEED</strong></h2>
                    <hr class="botm-line">
                    <script src="//lightwidget.com/widgets/lightwidget.js"></script>
                    <iframe src="//lightwidget.com/widgets/6ef13092da3f5877bdf509fad1c01e84.html" scrolling="no"
                            allowtransparency="true" class="lightwidget-widget"
                            style="width: 100%; border: 0; overflow: hidden; height:300px;"></iframe>
                    <script src="//lightwidget.com/widgets/lightwidget.js"></script>
                    <iframe src="//lightwidget.com/widgets/e609f3fba1235e2fa65f97cc8c6c1b34.html" scrolling="no"
                            allowtransparency="true" class="lightwidget-widget"
                            style="width: 100%; border: 0; overflow: hidden;height:300px;"></iframe>
                    <script src="//lightwidget.com/widgets/lightwidget.js"></script>
                    <iframe src="//lightwidget.com/widgets/e738ff180b61560e955e7c1a7c8afeb4.html" scrolling="no"
                            allowtransparency="true" class="lightwidget-widget"
                            style="width: 100%; border: 0; overflow: hidden;height:300px;"></iframe>
                    <script src="//lightwidget.com/widgets/lightwidget.js"></script>
                    <iframe src="//lightwidget.com/widgets/cd412ca3520a56b997efb1d86f08fa6f.html" scrolling="no"
                            allowtransparency="true" class="lightwidget-widget"
                            style="width: 100%; border: 0; overflow: hidden;height:300px;"></iframe>
                </div>
            </div>
            <div class="col-md-9 col-sm-12">
                <h1><strong>SERVICES</strong></h1>
                <BR><BR>
                <h3 style="font-size:42px;"><strong>KEYNOTE SPEAKING | FIRESIDE CHATS</strong></h3>
                <BR>
                <p style="font-size:32px;">
                    Open Minded, not scared to speak the truth Saqib is one of the most entertaining
                    speakers you can find. His youth attitude coupled with thought provoking concepts will
                    leave your audiences in awe. Saqib will completely leave your audience motivated,
                    entertained and crave for more.
                </p>
                <BR><BR>
                <h3 style="font-size:42px;"><strong>The #ASKTHEKHAN Live show | Q&A: </strong></h3>
                <BR>
                <p style="font-size:32px;">Q&A is probably one thing which Values a lot. Saqib personally believed that
                    it is not
                    really about the knowledge, it is about the right questions and the perfect answers to
                    those questions. It doesn’t matter whether you are a college, a startup, a business house
                    or a well settled corporate firm, If you have questions, Saqib will answer them LIVE.
                </p>
                <BR><BR>
                <h3 style="font-size:42px"><strong>SEMINARS | WORKSHOPS</strong></h3>
                <BR>
                <p style="font-size:32px;">From Business leadership to corporate world, from placements to excelling
                    your career,
                    from Entrepreneurship to making life choices, over 50 topics for you to choose from and
                    arrange a one of it’s kind workshop in your institution/organization. It doesn’t end there,
                    have a requirement? Set up a meet with Saqib Khan himself and speak about the
                    requirement and he would create a custom session catering to the demand of the batch.
                </p>
                <hr>
            </div>
            <!--/#portfolio-item-->
            <!--End of yousection-->
        </div>
    </div>
</section>
<!--Form Section Starts-->
<section style="background-image:url(img/iplink.png); background-size:cover;">
    <div>
        <div class="container">
            <div class="center">
                <h1>WANT TO GET IN TOUCH WITH ME?</h1>
            </div>
            <div class="row contact-wrap">
                <div class="status alert alert-success" style="display: none"></div>
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="form-group">
                            <label>Name *</label>
                            <input type="text" name="Name" width="50%" name="name" class="form-control"
                                   required="required">
                        </div>
                        <div class="form-group">
                            <label>Organization/Institute*</label>
                            <input type="text" name="college" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Email *</label>
                            <input type="email" name="email" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                            <label>Phone*</label>
                            <input type="number" name="phone" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label for="sel1">Purpose: </label>
                            <select class="form-control" id="sel1">
                                <option>General Enquiry</option>
                                <option>Hire to talk</option>
                                <option>Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Description*</label>
                            <textarea name="message" id="message" required="required" class="form-control"
                                      rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-primary btn-lg">Submit Message</button>
                        </div>
                    </div>
            </div>
            </form>
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</section>
<!--Form Section Ends-->
<!--START OF you SECTION -->
<section id="portfolio">
    <
    <div class="col-md-12">
        <div class="container">
            <h1>
                <center>TALKS TO WATCH</center>
            </h1>
            <hr class="small">
            <!--   <span class="subheading">Trending posts</span> -->
        </div>
    </div>
    </div>
    <div class="portfolio-items">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="video-container">
                    <iframe width="290" height="240" style="padding-left:20px;padding-right:20px"
                            src="https://www.youtube.com/embed/6rYxQg-2crU" frameborder="0" allowfullscreen></iframe>
                </div>
                <br>
                <br>
            </div>
            <!--/.portfolio-item-->
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="video-container">
                    <iframe width="290" height="240" style="padding-left:2px;padding-right:20px"
                            src="https://www.youtube.com/embed/oyOkDtfn4A4" frameborder="0" allowfullscreen></iframe>
                </div>
                <br>
                <br>
            </div>
            <!--/.portfolio-item-->
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="video-container">
                    <iframe width="290" height="240" style="padding-left:2px;padding-right:20px"
                            src="https://www.youtube.com/embed/iYN5gAqkJJU" frameborder="0" allowfullscreen></iframe>
                </div>
                <br>
                <br>
            </div>
            <!--/.portfolio-item-->
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="video-container">
                    <iframe width="290" height="240" style="padding-left:2px;padding-right:20px"
                            src="https://www.youtube.com/embed/i3HI2EJDQpk" frameborder="0" allowfullscreen></iframe>
                </div>
                <br>
                <br>
            </div>
            <!--/.portfolio-item-->
        </div>
        <!--row-->
    </div>
    <!--/.portfolio-item-->
    </div>
    </b>
    </div>
</section>
<!--/#portfolio-item-->
<!--End of yousection-->
<!--Social Media Icon Section Starts-->
<div class="container-fluid">
    <div class="row" style="background-color:black">
        <div class="col-sm-2">
            <img src="img\social icons\getintouch.png"></img>
        </div>
        <div class="col-sm-2 card">
            <a href="https://www.facebook.com/saqibkhanspeaks/">
                <img src="img\social icons\fw.png" alt="Card Back">
                <img src="img\social icons\fc.png" class="img-top" alt="Card Front">
            </a>
        </div>
        <div class="col-sm-2 card">
            <a href="https://www.instagram.com/saqibkhanspeaks/">
                <img src="img\social icons\iw.png" alt="Card Back">
                <img src="img\social icons\ic.png" class="img-top" alt="Card Front">
            </a>
        </div>
        <div class="col-sm-2 card">
            <a href="https://www.snapchat.com/add/saqibkhanspeaks">
                <img src="img\social icons\sw.png" alt="Card Back">
                <img src="img\social icons\sc.png" class="img-top" alt="Card Front">
            </a>
        </div>
        <div class="col-sm-2 card">
            <a href="https://twitter.com/saqibkhanspeaks">
                <img src="img\social icons\tw.png" alt="Card Back">
                <img src="img\social icons\tc.png" class="img-top" alt="Card Front">
            </a>
        </div>
        <div class="col-sm-2 card">
            <a href="https://www.youtube.com/c/SaqibKhanSpeaks">
                <img src="img\social icons\yw.png" alt="Card Back">
                <img src="img\social icons\yc.png" class="img-top" alt="Card Front">
            </a>
        </div>
    </div>
</div>
<!--Social Media Icon Section Starts-->
<?php include_once 'footer.php'; ?>
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>