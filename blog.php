<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'header.php'; ?>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/3/w3.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="css/blog.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body style="background-color:white;">
<nav role="navigation" class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="nav navbar-nav navbar-brand">
                <li class="sk">
                    <a href="index.php" class="brandsk" style="float:left;padding-top: 0px;padding-bottom: 15px;">SAQIB
                        KHAN</a>
                </li>
            </ul>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="blog.php">&nbsp;&nbsp;&nbsp;BLOG&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="punch_it.php">&nbsp;&nbsp;&nbsp;PUNCH IT&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="invite.php">&nbsp;&nbsp;&nbsp;INVITE TO SPEAK&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="lets_meet.php"> &nbsp;&nbsp;&nbsp;LET'S MEET&nbsp;&nbsp;&nbsp;</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<!-- First Blog Post -->
<div>
    <p>
        <a href="MY WEBSITE LINK" target="_blank">
            <center><img src="img/blog/wrong.jpeg" style='width:95%;' class="responsive-image" border="0" alt="Null">
            </center>
        </a>
    </p>
    <br>
    <div class="col-md-6"
    "col-sm-3">
    <h2>
        <a href="#">
            <center>Something is wrong</center>
        </a>
    </h2>
</div>
<br>
<div id="f1" class="col-md-6"
"col-sm-3>
<ul>
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://bit.ly/SYO3LX"><i style="background:#3b5998"
                                                                                       class="fa fa-facebook"></i></a>
    </li>
    <li>
        <a href="https://twitter.com/home?status=7%20Marketing%20Skills%20You%20Need%20To%20Get%20a%20Promotion:%20http://bit.ly/SYO3LX%20via%20@HubSpot%20%23marketingtips"><i
                    style="background:#00aced" class="fa fa-twitter"></i></a></li>
    <li>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://bit.ly/SYO3LX&title=7%20Marketing%20Skills%20You%20Need%20To%20Get%20A%20Promotion&summary=Are%20you%20vying%20for%20a%20promotion?%20If%20so,%20you%20should%20make%20sure%20you're%20doing%20everything%20you%20can%20to%20impress%20your%20boss.%20If%20you%20can%20master%20these%20marketing%20skills,%20you'll%20be%20in%20a%20much%20better%20place%20when%20management%20considers%20who%20to%20promote.%20http://bit.ly/SYO3LX&source="><i
                    style="background:#00aced" class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
<br><br><br>
<hr>
<!-- Navigation -->
<!-- /.container -->
</nav>
<!-- Page Content -->
<!-- Page Content -->
<div class="container">
    <div class="row1">
        <!-- Blog Entries Column -->
        <div class="col-md-8"
        "col-sm-3">
        <!-- Second Blog Post -->
        <div>
            <p>
                <a href="MY WEBSITE LINK" target="_blank">
                    <img src="img/blog/happy.jpeg" style='width:100%;' class="responsive-image" border="0" alt="Null">
                </a>
            </p>
            <div class="col-md-4"
            "col-sm-3">
            <h2 class="post-title"><a href="#">Be happy with yourself</a></h2>
        </div>
        <br>
        <div id="f1" class="col-md-8"
        "col-sm-3>
        <ul>
            <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://bit.ly/SYO3LX"><i
                            style="background:#3b5998" class="fa fa-facebook"></i></a></li>
            <li>
                <a href="https://twitter.com/home?status=7%20Marketing%20Skills%20You%20Need%20To%20Get%20a%20Promotion:%20http://bit.ly/SYO3LX%20via%20@HubSpot%20%23marketingtips"><i
                            style="background:#00aced" class="fa fa-twitter"></i></a></li>
            <li>
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://bit.ly/SYO3LX&title=7%20Marketing%20Skills%20You%20Need%20To%20Get%20A%20Promotion&summary=Are%20you%20vying%20for%20a%20promotion?%20If%20so,%20you%20should%20make%20sure%20you're%20doing%20everything%20you%20can%20to%20impress%20your%20boss.%20If%20you%20can%20master%20these%20marketing%20skills,%20you'll%20be%20in%20a%20much%20better%20place%20when%20management%20considers%20who%20to%20promote.%20http://bit.ly/SYO3LX&source="><i
                            style="background:#00aced" class="fa fa-linkedin"></i></a></li>
        </ul>
    </div>
</div>
<br><br><br>
<!-- Third Blog Post -->
<hr>
<div>
    <p>
        <a href="MY WEBSITE LINK" target="_blank">
            <img src="img/blog/say no.jpg" style='width:100%;' class="responsive-image" border="0" alt="Null">
        </a>
    </p>
    <div class="col-md-4"
    "col-sm-3">
    <h2 class="post-title"><a href="#">Learn to say no</a></h2>
</div>
<br>
<div id="f1" class="col-md-8"
"col-sm-3>
<ul>
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://bit.ly/SYO3LX"><i style="background:#3b5998"
                                                                                       class="fa fa-facebook"></i></a>
    </li>
    <li>
        <a href="https://twitter.com/home?status=7%20Marketing%20Skills%20You%20Need%20To%20Get%20a%20Promotion:%20http://bit.ly/SYO3LX%20via%20@HubSpot%20%23marketingtips"><i
                    style="background:#00aced" class="fa fa-twitter"></i></a></li>
    <li>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://bit.ly/SYO3LX&title=7%20Marketing%20Skills%20You%20Need%20To%20Get%20A%20Promotion&summary=Are%20you%20vying%20for%20a%20promotion?%20If%20so,%20you%20should%20make%20sure%20you're%20doing%20everything%20you%20can%20to%20impress%20your%20boss.%20If%20you%20can%20master%20these%20marketing%20skills,%20you'll%20be%20in%20a%20much%20better%20place%20when%20management%20considers%20who%20to%20promote.%20http://bit.ly/SYO3LX&source="><i
                    style="background:#00aced" class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
<br><br><br>
<!-- Forth Blog Post -->
<hr>
<div>
    <p>
        <a href="MY WEBSITE LINK" target="_blank">
            <img src="img/blog/male.jpeg" style='width:100%;' class="responsive-image" border="0" alt="Null">
        </a>
    </p>
    <div class="col-md-4"
    "col-sm-3">
    <h2 class="post-title"><a href="#">Let's think of male community too</a></h2>
</div>
<br>
<div id="f1" class="col-md-8"
"col-sm-3>
<ul>
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://bit.ly/SYO3LX"><i style="background:#3b5998"
                                                                                       class="fa fa-facebook"></i></a>
    </li>
    <li>
        <a href="https://twitter.com/home?status=7%20Marketing%20Skills%20You%20Need%20To%20Get%20a%20Promotion:%20http://bit.ly/SYO3LX%20via%20@HubSpot%20%23marketingtips"><i
                    style="background:#00aced" class="fa fa-twitter"></i></a></li>
    <li>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://bit.ly/SYO3LX&title=7%20Marketing%20Skills%20You%20Need%20To%20Get%20A%20Promotion&summary=Are%20you%20vying%20for%20a%20promotion?%20If%20so,%20you%20should%20make%20sure%20you're%20doing%20everything%20you%20can%20to%20impress%20your%20boss.%20If%20you%20can%20master%20these%20marketing%20skills,%20you'll%20be%20in%20a%20much%20better%20place%20when%20management%20considers%20who%20to%20promote.%20http://bit.ly/SYO3LX&source="><i
                    style="background:#00aced" class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
<br><br><br>
<!-- Fifth Blog Post -->
<hr>
<div>
    <p>
        <a href="MY WEBSITE LINK" target="_blank">
            <img src="img/blog/failure.jpg" style='width:100%;' class="responsive-image" border="0" alt="Null">
        </a>
    </p>
    <div class="col-md-4"
    "col-sm-3">
    <h2 class="post-title"><a href="#">The reason you fail</a></h2>
</div>
<br>
<div id="f1" class="col-md-8"
"col-sm-3>
<ul>
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=http://bit.ly/SYO3LX"><i style="background:#3b5998"
                                                                                       class="fa fa-facebook"></i></a>
    </li>
    <li>
        <a href="https://twitter.com/home?status=7%20Marketing%20Skills%20You%20Need%20To%20Get%20a%20Promotion:%20http://bit.ly/SYO3LX%20via%20@HubSpot%20%23marketingtips"><i
                    style="background:#00aced" class="fa fa-twitter"></i></a></li>
    <li>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://bit.ly/SYO3LX&title=7%20Marketing%20Skills%20You%20Need%20To%20Get%20A%20Promotion&summary=Are%20you%20vying%20for%20a%20promotion?%20If%20so,%20you%20should%20make%20sure%20you're%20doing%20everything%20you%20can%20to%20impress%20your%20boss.%20If%20you%20can%20master%20these%20marketing%20skills,%20you'll%20be%20in%20a%20much%20better%20place%20when%20management%20considers%20who%20to%20promote.%20http://bit.ly/SYO3LX&source="><i
                    style="background:#00aced" class="fa fa-linkedin"></i></a></li>
</ul>
</div>
</div>
<br><br><br>
</div>
<div class="col-sm-4" style="background-color:black;">
    <h3><font color="white"><b>GET EVERYTHING EXCLUSIVELY IN YOUR MAILBOX</b></font></h3>
    <div class="col-lg-4 col-md-4 col-sm-6">
        <input class="input-lg" type="text" placeholder="Enter your name"><br><br>
        <input class="input-lg" type="text" placeholder="Enter email"><br><br>
        <div class="form-group">
            <center>
                <button type="submit" name="submit" class="btn btn-primary btn-lg"><font
                            color="white"><b>SUBMIT</b></font></button>
            </center>
        </div>
    </div>
    <br>
</div>
<div class="col-sm-4" style="background-color:white;"><br><br></div>
<div class="col-sm-4" style="background-color:black;">
    <div class="oldblog">
        <div class="row1">
            <ul class="list-unstyled">
                <li>
                    <img src="img/blog/history.jpeg" style='width:85%;' border="0" alt="Null"><br><br>
                    <h4><a href="#"><font color="white">A little mystery and a little history</font></a></h4>
                </li>
                <br>
                <li>
                    <img src="img/blog/history.jpeg" style='width:85%;' border="0" alt="Null"><br><br>
                    <h4><a href="#"><font color="white">A little mystery and a little history</font></a></h4>
                </li>
                <br>
                <li>
                    <img src="img/blog/history.jpeg" style='width:85%;' border="0" alt="Null"><br><br>
                    <h4><a href="#"><font color="white">A little mystery and a little history</font></a></h4>
                </li>
                <br>
                <li>
                    <img src="img/blog/history.jpeg" style='width:85%;' border="0" alt="Null"><br><br>
                    <h4><a href="#"><font color="white">A little mystery and a little history</font></a></h4>
                </li>
                <br>
            </ul>
        </div>
    </div>
</div>
</div>
</div>
<!-- /.row -->
<center>
    <div>
        <ul class="pagination pagination-lg">
            <li class="active"><a href="#">1</a></li>
            <li><a href="index.php">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
        </ul>
    </div>
</center>
<hr>
<br>
<div class="blackbox">
    <center>
        <div class="container2">
            <h1><b><font color="white">LET'S DISCUSS MORE OVER A COFFEE</font></b></h1>
            <br>
            <form method="post" action="lets_meet.php">
                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-primary btn-lg"><b>MEET ME PERSONALLY</b>
                    </button>
                </div>
            </form>
    </center>
</div>
</div>
<br>
<hr>
<?php include_once 'footer.php'; ?>
<!-- Bootstrap core JavaScript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>