<?php

require_once 'config.php';
require_once 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

foreach ($_REQUEST as $key => $value) {
    $$key = $value;
}

switch ($action) {
    case 'lets_meet':
        $Name = ucfirst(trim($name));
        $college = ucfirst(trim($college));
        $phone = trim($phone);
        $subject = 'Invite to Speak - ' . $sel1 . ' - From : ' . $Name . ' - Org : ' . $college . ' - Phone : ' . $phone;
        $res = sendMail(EMAIL, PASSWORD, $email, $Name, EMAIL, SAQIBKHAN, $subject, $message);
        if ($res) {
            addInvite($Name, $college, $email, $phone, $sel1, $message);
        }
        echo(json_encode($res));
        break;

    case 'submit_form':
        $Name = ucfirst(trim($Name));
        $college = ucfirst(trim($college));
        $phone = trim($phone);
        $subject = 'Invite to Speak - ' . $sel1 . ' - From : ' . $Name . ' - Org : ' . $college . ' - Phone : ' . $phone;
        $res = sendMail(EMAIL, PASSWORD, $email, $Name, EMAIL, SAQIBKHAN, $subject, $message);
        if ($res) {
            addInvite($Name, $college, $email, $phone, $sel1, $message);
        }
        echo(json_encode($res));
        //echo(json_encode(true));
        break;

    default:
        break;
}

function addInvite($Name, $college, $email, $phone, $sel1, $message)
{
    global $db;
    $invite = array('name' => $Name, 'org' => $college, 'email' => $email, 'phone' => $phone, 'reason' => $sel1, 'message' => $message, 'status' => 1, 'add_timestamp' => date('Y-m-d H:i:s'));
    $db->insert('invite', $invite);
    $invite_id = $db->lastid();
}

function sendMail($username, $password, $setFrom, $setFromName, $addAddress, $addAddressName, $body, $subject)
{
    $mail = new PHPMailer;

    //$mail->SMTPDebug = 3;									// Enable verbose debug output

    $mail->isSMTP();                                        // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';                    // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                                    // Enable SMTP authentication
    $mail->Username = $username;                            // SMTP username
    $mail->Password = $password;                            // SMTP password
    $mail->SMTPSecure = 'tls';                                // Enable TLS encryption, ssl also accepted
    $mail->Port = 587;                                        // TCP port to connect to

    $mail->setFrom($setFrom, $setFromName);
    $mail->addAddress($addAddress, $addAddressName);        // Add a recipient
    //$mail->addAddress('ellen@example.com');				// Name is optional
    $mail->addReplyTo($setFrom, $setFromName);
    // $mail->addCC('cc@example.com');
    // $mail->addBCC('bcc@example.com');

    // $mail->addAttachment('/var/tmp/file.tar.gz');		// Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');	// Optional name
    $mail->isHTML(true);                                    // Set email format to HTML

    $mail->Subject = $body;
    $mail->Body = $subject;
    $mail->AltBody = $subject;

    if (!$mail->send()) {
        echo('Mailer Error: ' . $mail->ErrorInfo);
        return false;
    } else
        return true;
}

?>