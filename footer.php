<!--Social Media Icon Section Starts-->
<div class="container-fluid">
    <div class="row" style="background-color:black">
        <div class="col-sm-2">
            <img src="img\social icons\getintouch.png"></img>
        </div>
        <div class="col-sm-2 card">
            <a href="https://www.facebook.com/saqibkhanspeaks/">
                <img src="img\social icons\fw.png" alt="Card Back">
                <img src="img\social icons\fc.png" class="img-top" alt="Card Front">
            </a>
        </div>
        <div class="col-sm-2 card">
            <a href="https://www.instagram.com/saqibkhanspeaks/">
                <img src="img\social icons\iw.png" alt="Card Back">
                <img src="img\social icons\ic.png" class="img-top" alt="Card Front">
            </a>
        </div>
        <div class="col-sm-2 card">
            <a href="https://www.snapchat.com/add/saqibkhanspeaks">
                <img src="img\social icons\sw.png" alt="Card Back">
                <img src="img\social icons\sc.png" class="img-top" alt="Card Front">
            </a>
        </div>
        <div class="col-sm-2 card">
            <a href="https://twitter.com/saqibkhanspeaks">
                <img src="img\social icons\tw.png" alt="Card Back">
                <img src="img\social icons\tc.png" class="img-top" alt="Card Front">
            </a>
        </div>
        <div class="col-sm-2 card">
            <a href="https://www.youtube.com/c/SaqibKhanSpeaks">
                <img src="img\social icons\yw.png" alt="Card Back">
                <img src="img\social icons\yc.png" class="img-top" alt="Card Front">
            </a>
        </div>
    </div>
</div>
<!--Social Media Icon Section Starts-->
<!-- Footer -->
<footer>
    <b>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <b>
                        <h3>Address</h3>
                    </b>
                    <b>
                        <p class="add">'B' Building, 3rd Floor, abcl Complex, Mumbai-400058</p>
                    </b>
                    <b>
                        <p class="add">022 789456123</p>
                    </b>
                    <b>
                        <p class="add">+91 789456123</p>
                    </b>
                </div>
                <div class="col-md-4 quickLinks">
                    <b>
                        <h3>Quick Links</h3>
                    </b>
                    <ul>
                        <b>
                            <li><a href="index.php">HOME</a></li>
                        </b>
                        <b>
                            <li><a href="blog.php">BLOG</a></li>
                        </b>
                        <b>
                            <li><a href="punch_it.php">PUNCH IT</a></li>
                        </b>
                        <b>
                            <li><a href="invite.php">INVITE ME TO SPEAK</a></li>
                        </b>
                        <b>
                            <li><a href="lets_meet.php">LETS MEET</a></li>
                        </b>
                    </ul>
                </div>
                <div class="col-md-4 socialIcon">
                    <b>
                        <h3>Developed by </h3>
                        <br>
                    </b>
                    <a href="http://www.engage.org.in/main/" target="_blank"><img src="img/ENGage.png"></a>
                </div>
            </div>
        </div>
        <div class="copy">
            <p>&copy; 2017 SaqibKhanSpeaks. All rights reserved.</p>
    </b>
    </div>
</footer>